--------------------------------
Introduction
--------------------------------
A Tic Tac Toe game made for University of Calgary ENSF 409 course.

The algorithm written in this application is not the best, but it was suitable for the project.

--------------------------------
License
--------------------------------
Copyright (C) 2014 Jack L (http://jack-l.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

--------------------------------
Code made public since February 02, 2014
Readme file updated on August 23, 2017