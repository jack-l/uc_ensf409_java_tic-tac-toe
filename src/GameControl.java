/*
 Copyright (C) 2014 Jack L (http://jack-l.com)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import javax.swing.JOptionPane;
import java.util.Random;

public class GameControl {

    //Variables
    protected String[] PlayerNames; //
    private String[] PlayerSymbols; //
    protected int GameMode;
    private int CurrentPlayer; // 
    private short MovementsCount; //
    private Color WinButtonColor;

    public GameControl() {
        //Allocate variables
        PlayerSymbols = new String[2];
        PlayerNames = new String[2];
        //Initialize variables
        PlayerSymbols[0] = "X";
        PlayerSymbols[1] = "O";
        GameMode = 0;
        MovementsCount = 0;
        CurrentPlayer = 0; //0 is player 1, 1 is player 2
        WinButtonColor = new Color(0, 191, 255);
    }

    public void Move(short x, short y) { // Must send in button index
        if (GameMode == 11) { //versus random computer
            //Player's turn
            mainFrame.TheGame.cmdGameBoard[x][y].setText(PlayerSymbols[CurrentPlayer]);
            mainFrame.TheGame.cmdGameBoard[x][y].setForeground(Color.RED);
            CurrentPlayer++; //Switch player
            MovementsCount++; //Count movements for player
            //It is CPU's turn
            AIPickRandomSpot();
            MovementsCount++; //Switch player
            CurrentPlayer--; //Count movements for CPU
        } //****************************************************************
        else if (GameMode == 12) { //versus blocking computer
            //Player's turn
            mainFrame.TheGame.cmdGameBoard[x][y].setText(PlayerSymbols[CurrentPlayer]);
            mainFrame.TheGame.cmdGameBoard[x][y].setForeground(Color.RED);
            CurrentPlayer++; //Switch player
            MovementsCount++; //Count movements for player
            //It is CPU's turn
            AIBlockingSpot();
            MovementsCount++; //Switch player
            CurrentPlayer--; //Count movements for CPU
        } //****************************************************************
        else if (GameMode == 13) { //versus smarter computer
            //Player's turn
            mainFrame.TheGame.cmdGameBoard[x][y].setText(PlayerSymbols[CurrentPlayer]);
            mainFrame.TheGame.cmdGameBoard[x][y].setForeground(Color.RED);
            CurrentPlayer++; //Switch player
            MovementsCount++; //Count movements for player
            //It is CPU's turn
            AISmartSpot();
            MovementsCount++; //Switch player
            CurrentPlayer--; //Count movements for CPU
        } //****************************************************************
        else if (GameMode == 2) { //versus another player
            mainFrame.TheGame.cmdGameBoard[x][y].setText(PlayerSymbols[CurrentPlayer]);
            if (CurrentPlayer == 0) {
                mainFrame.TheGame.cmdGameBoard[x][y].setForeground(Color.RED);
            } //Set red for player 1, no nothing for player2
            //Switch player
            if (CurrentPlayer == 0) {
                CurrentPlayer++;
            } else if (CurrentPlayer == 1) {
                CurrentPlayer = 0;
            }
            MovementsCount++; //Countmovements
        } else {
            JOptionPane.showMessageDialog(null, "Please create a new game first!", "Warning: ", JOptionPane.WARNING_MESSAGE);
            return;
        }
        //Check if we have a winner or not
        short Winner = CheckWin();
        if (Winner == 1) {
            GameMode = 0; //Disable game by setting game mode to 0
            JOptionPane.showMessageDialog(null, "Player " + PlayerNames[0] + " is the winner!\nYou may restart the game by clicking New Game button.", "Game Ended: ", JOptionPane.INFORMATION_MESSAGE);
        } else if (Winner == 2) {
            GameMode = 0; //Disable game by setting game mode to 0
            JOptionPane.showMessageDialog(null, "Player " + PlayerNames[1] + " is the winner!\nYou may restart the game by clicking New Game button.", "Game Ended: ", JOptionPane.INFORMATION_MESSAGE);
        } else if (Winner == 3) {
            GameMode = 0; //Disable game by setting game mode to 0
            JOptionPane.showMessageDialog(null, "Game Draw!\nYou may restart the game by clicking New Game button.", "Game Ended: ", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private short CheckWin() { // 0 - no winner, contine, 1 - player 1 is winner, 2 - player 2 is winner, 3 - Draw
        if (MovementsCount < 5) {
            return 0; //No one can win in less than 5 movements, just ignore
        }
        if (!mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
            if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[0][1].getText()) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[0][2].getText())) {
                // ***
                // ---
                // ---
                //Change button color
                mainFrame.TheGame.cmdGameBoard[0][0].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[0][1].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[0][2].setBackground(WinButtonColor);
                //Return the winner player
                if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                    return 1;
                } else if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                    return 2;
                }
            } else if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[1][0].getText()) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[2][0].getText())) {
                // *--
                // *--
                // *--
                //Change button color
                mainFrame.TheGame.cmdGameBoard[0][0].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[1][0].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[2][0].setBackground(WinButtonColor);
                //Return the winner player
                if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                    return 1;
                } else if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                    return 2;
                }
            } else if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[1][1].getText()) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[2][2].getText())) {
                // *--
                // -*-
                // --*
                //Change button color
                mainFrame.TheGame.cmdGameBoard[0][0].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[1][1].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[2][2].setBackground(WinButtonColor);
                //Return the winner player
                if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                    return 1;
                } else if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                    return 2;
                }
            }
        }

        if (!mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
            if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[1][0].getText()) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[1][2].getText())) {
                // ---
                // ***
                // ---
                //Change button color
                mainFrame.TheGame.cmdGameBoard[1][0].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[1][1].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[1][2].setBackground(WinButtonColor);
                //Return the winner player
                if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                    return 1;
                } else if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                    return 2;
                }
            } else if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[0][1].getText()) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[2][1].getText())) {
                // -*-
                // -*-
                // -*-
                //Change button color
                mainFrame.TheGame.cmdGameBoard[0][1].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[1][1].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[2][1].setBackground(WinButtonColor);
                //Return the winner player
                if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                    return 1;
                } else if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                    return 2;
                }
            } else if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[0][2].getText()) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[2][0].getText())) {
                // --*
                // -*-
                // *--
                //Change button color
                mainFrame.TheGame.cmdGameBoard[0][2].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[1][1].setBackground(WinButtonColor);
                mainFrame.TheGame.cmdGameBoard[2][0].setBackground(WinButtonColor);
                //Return the winner player
                if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                    return 1;
                } else if (mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                    return 2;
                }
            }

            if (!mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                if (mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[2][0].getText()) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[2][1].getText())) {
                    // ---
                    // ---
                    // ***
                    //Change button color
                    mainFrame.TheGame.cmdGameBoard[2][0].setBackground(WinButtonColor);
                    mainFrame.TheGame.cmdGameBoard[2][1].setBackground(WinButtonColor);
                    mainFrame.TheGame.cmdGameBoard[2][2].setBackground(WinButtonColor);
                    //Return the winner player
                    if (mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                        return 1;
                    } else if (mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                        return 2;
                    }

                } else if (mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[0][2].getText()) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(mainFrame.TheGame.cmdGameBoard[1][2].getText())) {
                    // --*
                    // --*
                    // --*
                    //Change button color
                    mainFrame.TheGame.cmdGameBoard[0][2].setBackground(WinButtonColor);
                    mainFrame.TheGame.cmdGameBoard[1][2].setBackground(WinButtonColor);
                    mainFrame.TheGame.cmdGameBoard[2][2].setBackground(WinButtonColor);
                    //Return the winner player
                    if (mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0])) {
                        return 1;
                    } else if (mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1])) {
                        return 2;
                    }
                }
            }
        }

        //If no condition math above and the board is filled
        if (MovementsCount >= 9) {
            return 3;
        }

        return 0; //No winner continue
    }

    private void AISmartSpot() { //Same as blocking AI except looks at board, if it can find a move to win immediately, it makes that move before anything else.
        if (CurrentPlayer != 1) {
            JOptionPane.showMessageDialog(null, "Error in CurrentPlayer variable.", "Error: ", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        if (CheckWin() != 0 || GameMode == 0) {
            return;
        }
        int Row = -1;
        int Col = -1;
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase(PlayerSymbols[1])) {
            if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase("")) {
                // **-
                // ---
                // ---
                mainFrame.TheGame.cmdGameBoard[0][2].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
                // -**
                // ---
                // ---
                mainFrame.TheGame.cmdGameBoard[0][0].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
                // -*-
                // ---
                // -*-
                mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
                Row = 1;
                Col = 1;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase(PlayerSymbols[1])) {
            if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase("")) {
                // *--
                // *--
                // ---
                mainFrame.TheGame.cmdGameBoard[2][0].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
                // ---
                // *--
                // *--
                mainFrame.TheGame.cmdGameBoard[0][0].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
                // ---
                // *-*
                // ---
                mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
                Row = 1;
                Col = 1;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase(PlayerSymbols[1])) {
            if (mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                // ---
                // ---
                // **-
                mainFrame.TheGame.cmdGameBoard[2][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase("")) {
                // ---
                // ---
                // -**
                mainFrame.TheGame.cmdGameBoard[2][0].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase(PlayerSymbols[1])) {
            if (mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                // --*
                // --*
                // ---
                mainFrame.TheGame.cmdGameBoard[2][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase("")) {
                // ---
                // --*
                // --*
                mainFrame.TheGame.cmdGameBoard[0][2].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 2;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[1])) {
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                // *--
                // -*-
                // ---
                mainFrame.TheGame.cmdGameBoard[2][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
                // ---
                // -*-
                // --*
                mainFrame.TheGame.cmdGameBoard[0][0].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 0;
            }
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase("")) {
                // --*
                // -*-
                // ---
                mainFrame.TheGame.cmdGameBoard[2][0].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase("")) {
                // ---
                // -*-
                // *--
                mainFrame.TheGame.cmdGameBoard[0][2].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 2;
            }
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase("")) {
                // ---
                // **-
                // ---
                mainFrame.TheGame.cmdGameBoard[1][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase("")) {
                // ---
                // -**
                // ---
                mainFrame.TheGame.cmdGameBoard[1][0].setText(PlayerSymbols[1]);
                Row = 1;
                Col = 0;
            }
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase("")) {
                // -*-
                // -*-
                // ---
                mainFrame.TheGame.cmdGameBoard[2][1].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 1;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase("")) {
                // ---
                // -*-
                // -*-
                mainFrame.TheGame.cmdGameBoard[0][1].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 1;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase("")) {
            // *-*
            // ---
            // ---
            mainFrame.TheGame.cmdGameBoard[0][1].setText(PlayerSymbols[1]);
            Row = 0;
            Col = 1;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase("")) {
            // *--
            // ---
            // *--
            mainFrame.TheGame.cmdGameBoard[1][0].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 0;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase("")) {
            // --*
            // ---
            // --*
            mainFrame.TheGame.cmdGameBoard[1][2].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 2;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase("")) {
            // ---
            // ---
            // *-*
            mainFrame.TheGame.cmdGameBoard[2][1].setText(PlayerSymbols[1]);
            Row = 2;
            Col = 1;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
            // *--
            // ---
            // --*
            mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 1;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[1]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
            // --*
            // ---
            // *--
            mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 1;
        }
        if (Row == -1 && Col == -1) {
            //If non of the above, check blocking then
            AIBlockingSpot();
        }
    }

    private void AIBlockingSpot() { //looks at the board for a move that would block its opponent from winning on the next move. If it can't find any such move, it picks a vacant square at random.
        if (CurrentPlayer != 1) {
            JOptionPane.showMessageDialog(null, "Error in CurrentPlayer variable.", "Error: ", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        if (CheckWin() != 0 || GameMode == 0) {
            return;
        }
        int Row = -1;
        int Col = -1;
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase(PlayerSymbols[0])) {
            if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase("")) {
                // **-
                // ---
                // ---
                mainFrame.TheGame.cmdGameBoard[0][2].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
                // -**
                // ---
                // ---
                mainFrame.TheGame.cmdGameBoard[0][0].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
                // -*-
                // ---
                // -*-
                mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
                Row = 1;
                Col = 1;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase(PlayerSymbols[0])) {
            if (mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase("")) {
                // *--
                // *--
                // ---
                mainFrame.TheGame.cmdGameBoard[2][0].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
                // ---
                // *--
                // *--
                mainFrame.TheGame.cmdGameBoard[0][0].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
                // ---
                // *-*
                // ---
                mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
                Row = 1;
                Col = 1;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase(PlayerSymbols[0])) {
            if (mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                // ---
                // ---
                // **-
                mainFrame.TheGame.cmdGameBoard[2][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase("")) {
                // ---
                // ---
                // -**
                mainFrame.TheGame.cmdGameBoard[2][0].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase(PlayerSymbols[0])) {
            if (mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                // --*
                // --*
                // ---
                mainFrame.TheGame.cmdGameBoard[2][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase("")) {
                // ---
                // --*
                // --*
                mainFrame.TheGame.cmdGameBoard[0][2].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 2;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase(PlayerSymbols[0])) {
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase("")) {
                // *--
                // -*-
                // ---
                mainFrame.TheGame.cmdGameBoard[2][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 2;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase("")) {
                // ---
                // -*-
                // --*
                mainFrame.TheGame.cmdGameBoard[0][0].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 0;
            }
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase("")) {
                // --*
                // -*-
                // ---
                mainFrame.TheGame.cmdGameBoard[2][0].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase("")) {
                // ---
                // -*-
                // *--
                mainFrame.TheGame.cmdGameBoard[0][2].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 2;
            }
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase("")) {
                // ---
                // **-
                // ---
                mainFrame.TheGame.cmdGameBoard[1][2].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 0;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase("")) {
                // ---
                // -**
                // ---
                mainFrame.TheGame.cmdGameBoard[1][0].setText(PlayerSymbols[1]);
                Row = 1;
                Col = 0;
            }
            if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase("")) {
                // -*-
                // -*-
                // ---
                mainFrame.TheGame.cmdGameBoard[2][1].setText(PlayerSymbols[1]);
                Row = 2;
                Col = 1;
            } else if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase("")) {
                // ---
                // -*-
                // -*-
                mainFrame.TheGame.cmdGameBoard[0][1].setText(PlayerSymbols[1]);
                Row = 0;
                Col = 1;
            }
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[0][1].getText().equalsIgnoreCase("")) {
            // *-*
            // ---
            // ---
            mainFrame.TheGame.cmdGameBoard[0][1].setText(PlayerSymbols[1]);
            Row = 0;
            Col = 1;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][0].getText().equalsIgnoreCase("")) {
            // *--
            // ---
            // *--
            mainFrame.TheGame.cmdGameBoard[1][0].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 0;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][2].getText().equalsIgnoreCase("")) {
            // --*
            // ---
            // --*
            mainFrame.TheGame.cmdGameBoard[1][2].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 2;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][1].getText().equalsIgnoreCase("")) {
            // ---
            // ---
            // *-*
            mainFrame.TheGame.cmdGameBoard[2][1].setText(PlayerSymbols[1]);
            Row = 2;
            Col = 1;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
            // *--
            // ---
            // --*
            mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 1;
        }
        if (Row == -1 && mainFrame.TheGame.cmdGameBoard[0][2].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[2][0].getText().equalsIgnoreCase(PlayerSymbols[0]) && mainFrame.TheGame.cmdGameBoard[1][1].getText().equalsIgnoreCase("")) {
            // --*
            // ---
            // *--
            mainFrame.TheGame.cmdGameBoard[1][1].setText(PlayerSymbols[1]);
            Row = 1;
            Col = 1;
        }
        if (Row == -1 && Col == -1) {
            //If non of the above, let's place a random location
            AIPickRandomSpot();
        }
    }

    private void AIPickRandomSpot() { //Calculate a random spot for computer to move
        if (CurrentPlayer != 1) {
            JOptionPane.showMessageDialog(null, "Error in CurrentPlayer variable.", "Error: ", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        if (CheckWin() == 0 && GameMode != 0) { //Make sure win status is 0
            Random RndGen = new Random(); //Random number generator
            int Row = -1, Col;
            short EmptyRowSpace = 14; //Keep count of empty row space
            while (true) {
                /*
                First assume the whole board is empty, A row or a col can have a total of 8 possibilities.
                I use binary (1110) to represent all possibilities. 1 means the space is not taken, 0 means the space is taken. The first right bit is not in use.
                As calculation goes, the AI will either to find a spot that is avaliable or the spot is taken then mark space taken and recalculate for a new spot until an avaliable spot is found.
                Finally, the computer should able to find an empty slot for the move.
                */
                if (EmptyRowSpace == 14) {
                    Row = RndGen.nextInt(3);
                } else if (EmptyRowSpace == 12) {
                    Row = RndGen.nextInt(2) + 1;
                } else if (EmptyRowSpace == 10) {
                    Row = RndGen.nextInt(100) % 2;
                    if (Row == 1) {
                        Row = 2;
                    }
                } else if (EmptyRowSpace == 8) {
                    Row = 2;
                } else if (EmptyRowSpace == 6) {
                    Row = RndGen.nextInt(100) % 2;
                } else if (EmptyRowSpace == 4) {
                    Row = 1;
                } else if (EmptyRowSpace == 2) {
                    Row = 0;
                }

                //Check empty col space in the same row
                short EmptyColSpace = 0;
                if (mainFrame.TheGame.cmdGameBoard[Row][0].getText().equalsIgnoreCase("")) {
                    EmptyColSpace += 2;
                }
                if (mainFrame.TheGame.cmdGameBoard[Row][1].getText().equalsIgnoreCase("")) {
                    EmptyColSpace += 4;
                }
                if (mainFrame.TheGame.cmdGameBoard[Row][2].getText().equalsIgnoreCase("")) {
                    EmptyColSpace += 8;
                }
                //Make a movement according to empty col space in the row
                if (EmptyColSpace == 14) {
                    Col = RndGen.nextInt(3);
                    mainFrame.TheGame.cmdGameBoard[Row][Col].setText(PlayerSymbols[1]);
                    break;
                } else if (EmptyColSpace == 12) {
                    Col = RndGen.nextInt(2) + 1;
                    mainFrame.TheGame.cmdGameBoard[Row][Col].setText(PlayerSymbols[1]);
                    break;
                } else if (EmptyColSpace == 10) {
                    Col = RndGen.nextInt(2);
                    if (Col == 0) {
                        mainFrame.TheGame.cmdGameBoard[Row][0].setText(PlayerSymbols[1]);
                    } else {
                        mainFrame.TheGame.cmdGameBoard[Row][2].setText(PlayerSymbols[1]);
                        Col = 2;
                    }
                    break;
                } else if (EmptyColSpace == 8) {
                    Col = 2;
                    mainFrame.TheGame.cmdGameBoard[Row][2].setText(PlayerSymbols[1]);
                    break;
                } else if (EmptyColSpace == 6) {
                    Col = RndGen.nextInt(2);
                    mainFrame.TheGame.cmdGameBoard[Row][Col].setText(PlayerSymbols[1]);
                    break;
                } else if (EmptyColSpace == 4) {
                    Col = 1;
                    mainFrame.TheGame.cmdGameBoard[Row][Col].setText(PlayerSymbols[1]);
                    break;
                } else if (EmptyColSpace == 2) {
                    Col = 0;
                    mainFrame.TheGame.cmdGameBoard[Row][Col].setText(PlayerSymbols[1]);
                    break;
                } else if (EmptyColSpace == 0) { //No empty space in the row, find a different row instead
                    if (Row == 0) {
                        EmptyRowSpace -= 2;
                    } else if (Row == 1) {
                        EmptyRowSpace -= 4;
                    } else if (Row == 1) {
                        EmptyRowSpace -= 8;
                    }
                }
            }
        }
    }

    public void ResetBoard() {// Reset all board to initial condition
        CurrentPlayer = 0; //Reset current player to player 1
        MovementsCount = 0;
        for (int i = 0; i < mainFrame.TheGame.cmdGameBoard.length; i++) {
            for (int j = 0; j < mainFrame.TheGame.cmdGameBoard[i].length; j++) {
                mainFrame.TheGame.cmdGameBoard[i][j].setText(""); //Clear the text in the field
                mainFrame.TheGame.cmdGameBoard[i][j].setBackground(Color.WHITE); //reset button color
                mainFrame.TheGame.cmdGameBoard[i][j].setForeground(Color.BLACK);
            }
        }

    }
}
