/*
 Copyright (C) 2014 Jack L (http://jack-l.com)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.*;

public class mainFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    //Variables
    protected static mainFrame TheGame = new mainFrame();
    private JFrame TheFrame; //Main frame
    private Container FrameC; //Container for main frame
    protected JButton[][] cmdGameBoard; //buttons for the game
    private JPanel PanelGameBoard; //Panel for the game board
    //Control buttons
    private JPanel PanelControl; //Panel for control buttons
    private JButton cmdNewGame;
    private JButton cmdExit;
    //GameControl
    protected GameControl Game;
    private boolean Processing = false; //Ignore click when computer is calculating

    public mainFrame() {
        //Constructor
        //Initialize frame
        TheFrame = new JFrame("TicTacToe Game");
        TheFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        TheFrame.setSize(500, 320);
        TheFrame.setResizable(false);
        TheFrame.setLocationRelativeTo(null); //Start the frame in the center

        //Initialize Container
        FrameC = new Container();
        FrameC = getContentPane();
        //Initialize control buttons
        cmdNewGame = new JButton("New Game");
        cmdExit = new JButton("Exit Game");
        PanelControl = new JPanel();
        PanelControl.add(cmdNewGame);
        PanelControl.add(cmdExit);
        FrameC.add(PanelControl, BorderLayout.SOUTH); //Add to border

        //Initialize game control
        Game = new GameControl();

        //Initialize game board buttons
        GridLayout GameBoardLayout = new GridLayout(3, 3);
        cmdGameBoard = new JButton[3][3];
        PanelGameBoard = new JPanel(GameBoardLayout);
        for (short i = 0; i < cmdGameBoard.length; i++) {
            for (short j = 0; j < cmdGameBoard[i].length; j++) {
                cmdGameBoard[i][j] = new JButton(""); //Set text to empty
                cmdGameBoard[i][j].setFont(new Font("Arial", Font.BOLD, 90)); //Set font size
                cmdGameBoard[i][j].setBackground(Color.white); //Set to white color
                PanelGameBoard.add(cmdGameBoard[i][j]); //Add the button to panel
                //Add event listener to buttons
                final short tmp1 = i, tmp2 = j;
                cmdGameBoard[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (Processing == false) {
                            Processing = true; //Set to true immediately to ignore further clicks
                            if (Game.GameMode == 2 || (Game.GameMode >= 11 && Game.GameMode <= 13)) {
                                if (!cmdGameBoard[tmp1][tmp2].getText().equalsIgnoreCase("")) {
                                    //Check if this space is taken or not
                                    JOptionPane.showMessageDialog(null, "This space is taken, please choose something else.", "Warning: ", JOptionPane.WARNING_MESSAGE);
                                } else {
                                    Game.Move(tmp1, tmp2); //Call move function
                                }
                                Processing = false; //Set to false alow new click further clicks
                            } else {
                                JOptionPane.showMessageDialog(null, "Please create a new game first!", "Warning: ", JOptionPane.WARNING_MESSAGE);
                            }
                            Processing = false; //Set to false to allow further clicks
                        } else {
                            JOptionPane.showMessageDialog(null, "You are clicking too fast! Please slow down! Drink some coffee!", "Warning: ", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                });
            }
        }
        FrameC.add(PanelGameBoard, BorderLayout.CENTER); //Add to center
        //
        TheFrame.add(FrameC); //Add container to frame

        //This part is just for fun!
        final Timer FunColor = new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Game.GameMode != 0) {
                    return;
                }
                Random RndGen = new Random();
                int Row = RndGen.nextInt(3);
                int Col = RndGen.nextInt(3);
                TheGame.cmdGameBoard[Row][Col].setBackground(new Color(RndGen.nextInt(256), RndGen.nextInt(256), RndGen.nextInt(256)));
                Row = RndGen.nextInt(3);
                Col = RndGen.nextInt(3);
                TheGame.cmdGameBoard[Row][Col].setBackground(new Color(RndGen.nextInt(256), RndGen.nextInt(256), RndGen.nextInt(256)));
                Row = RndGen.nextInt(3);
                Col = RndGen.nextInt(3);
                TheGame.cmdGameBoard[Row][Col].setBackground(new Color(RndGen.nextInt(256), RndGen.nextInt(256), RndGen.nextInt(256)));
            }
        });
        FunColor.start();
        //*************************************************************************
        //Create event listener
        //New game Button
        cmdNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String[] Options = {"Player VS. CPU", "Player VS. Player", "Cancel"};
                int UserSelect = JOptionPane.showOptionDialog(null, "Please select a game mode.", "New Game: ", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, Options, Options[0]);
                if (UserSelect == 0) { // vs. computer
                    //Ask for level of computer
                    Options = new String[4];
                    Options[0] = "Random AI";
                    Options[1] = "Blocking AI";
                    Options[2] = "Smart AI";
                    Options[3] = "Cancel";
                    UserSelect = JOptionPane.showOptionDialog(null, "Please select a computer difficulty.\n1. Random AI: Looks at the board, and picks a vacant square at random.\n2. Blocking AI: Will first looks at the board for a move that would block its opponent from winning on the next move. If it can't find any such move, it picks a vacant square at random.\n3. Smarter AI: Same as blocking AI except looks at board, if it can find a move to win immediately, it makes that move before anything else.", "Choose an AI difficulty:", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, Options, Options[0]);
                    if (UserSelect == 0) {
                        Game.PlayerNames[0] = JOptionPane.showInputDialog("Please enter your name: ");
                        if (Game.PlayerNames[0] != null) {
                            Game.PlayerNames[1] = "CPU";
                            Game.ResetBoard();
                            Game.GameMode = 11;
                            FunColor.stop();
                        }
                    } else if (UserSelect == 1) {
                        Game.PlayerNames[0] = JOptionPane.showInputDialog("Please enter your name: ");
                        if (Game.PlayerNames[0] != null) {
                            Game.PlayerNames[1] = "CPU";
                            Game.ResetBoard();
                            Game.GameMode = 12;
                            FunColor.stop();
                        }
                    } else if (UserSelect == 2) {
                        Game.PlayerNames[0] = JOptionPane.showInputDialog("Please enter your name: ");
                        if (Game.PlayerNames[0] != null) {
                            Game.PlayerNames[1] = "CPU";
                            Game.ResetBoard();
                            Game.GameMode = 13;
                            FunColor.stop();
                        }
                    }
                } else if (UserSelect == 1) { // vs. player
                    Game.PlayerNames[0] = JOptionPane.showInputDialog("Please enter your name: ");
                    if (Game.PlayerNames[0] != null) {
                        Game.PlayerNames[1] = JOptionPane.showInputDialog("Please enter you opponent's name: ");
                        if (Game.PlayerNames[1] != null) {
                            Game.ResetBoard();
                            Game.GameMode = 2;
                            FunColor.stop();
                        }
                    }
                }
            }
        });

        //Exit
        cmdExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                System.exit(0); //Exit application
            }
        });
    }

    //Main frame main function, start from here
    public static void main(String[] args) {
        TheGame.TheFrame.setVisible(true);
    }
}
